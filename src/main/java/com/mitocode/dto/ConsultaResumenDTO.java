package com.mitocode.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaResumenDTO {

	private Integer cantidad;
	private String fecha;
}
