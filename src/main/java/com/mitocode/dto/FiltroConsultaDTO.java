package com.mitocode.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FiltroConsultaDTO {

	private String dni;
	private String nombreCompleto;
}
