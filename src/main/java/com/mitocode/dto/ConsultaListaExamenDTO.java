package com.mitocode.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.mitocode.model.Consulta;
import com.mitocode.model.Examen;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConsultaListaExamenDTO {

	@NotNull
	private Consulta consulta;
	@NotNull
	private List<Examen> lstExamen;

}
