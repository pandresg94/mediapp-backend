package com.mitocode.exception;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExceptionResponse {

	private String mensaje;
	private String detalles;
	private LocalDateTime fecha;
}
