package com.mitocode.exception;

public class MediAppException extends Exception {

	private static final long serialVersionUID = 610147522529054652L;

	public MediAppException(String mensaje) {
		super(mensaje);
	}
}
