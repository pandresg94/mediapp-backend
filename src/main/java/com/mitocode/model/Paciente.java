package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "paciente")
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPaciente;

	@Size(min = 3, message = "{nombres.size}")
	@Column(name = "nombres", nullable = false, length = 60)
	private String nombres;
	@Size(min = 3, message = "{apellidos.size}")
	@Column(name = "apellidos", nullable = false, length = 60)
	private String apellidos;
	@Size(min = 8, message = "El dni debe tener minimo 8 caracteres")
	@Column(name = "dni", nullable = false, length = 10, unique = true)
	private String dni;
	@Column(name = "direccion", length = 150)
	private String direccion;
	@Column(name = "telefono", length = 15)
	private String telefono;
	@Email(message = "El email no tiene un formato de email correcto")
	@Column(name = "email", length = 60)
	private String email;

}
