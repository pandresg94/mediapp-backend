package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "medico")
public class Medico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMedico;

	@Column(name = "nombres", nullable = false, length = 70)
	private String nombre;

	@Column(name = "apellidos", nullable = false, length = 70)
	private String apellido;

	@Column(name = "cmp", nullable = false, length = 12, unique = true)
	private String cmp;

	@Column(name = "foto", nullable = false)
	private String fotoUrl;

}
