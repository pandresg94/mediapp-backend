package com.mitocode.service;

import java.util.List;

import com.mitocode.exception.MediAppException;
import com.mitocode.model.ConsultaExamen;

public interface IConsultaExamenService extends ICRUD<ConsultaExamen, Integer> {

	List<ConsultaExamen> listarExamenesPorConsulta(Integer idConsulta) throws MediAppException;
}
