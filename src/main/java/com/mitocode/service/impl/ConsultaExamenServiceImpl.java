package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.exception.MediAppException;
import com.mitocode.model.ConsultaExamen;
import com.mitocode.repo.IConsultaExamenRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IConsultaExamenService;

@Service
public class ConsultaExamenServiceImpl extends CRUDImpl<ConsultaExamen, Integer> implements IConsultaExamenService {

	@Autowired
	IConsultaExamenRepo repo;

	@Override
	protected IGenericRepo<ConsultaExamen, Integer> getRepo() {
		return repo;
	}

	@Override
	public List<ConsultaExamen> listarExamenesPorConsulta(Integer idConsulta) throws MediAppException {
		return repo.listarExamenesPorConsulta(idConsulta);
	}

}
