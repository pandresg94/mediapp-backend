package com.mitocode.service.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.exception.MediAppException;
import com.mitocode.model.Consulta;
import com.mitocode.repo.IConsultaExamenRepo;
import com.mitocode.repo.IConsultaRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IConsultaService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ConsultaServiceImpl extends CRUDImpl<Consulta, Integer> implements IConsultaService {

	@Autowired
	IConsultaRepo repo;

	@Autowired
	IConsultaExamenRepo ceRepo;

	@Override
	protected IGenericRepo<Consulta, Integer> getRepo() {
		return repo;
	}

	@Transactional
	@Override
	public Consulta registrarConsultaTransaccional(ConsultaListaExamenDTO consulta) throws MediAppException {
		// INSERTAR LA CONSULTA PARA OBTENER LA PK
		// INSERTAR DETALLE CONSULTA USANDO LA PK PREVIA
		try {
			consulta.getConsulta().getDetalleConsulta().forEach(det -> det.setConsulta(consulta.getConsulta()));
			repo.save(consulta.getConsulta());
			consulta.getLstExamen().forEach(ex -> {
				ceRepo.registrar(consulta.getConsulta().getIdConsulta(), ex.getIdExamen());
			});
			return consulta.getConsulta();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Consulta> buscar(FiltroConsultaDTO filtro) throws MediAppException {
		return repo.buscar(filtro.getDni(), filtro.getNombreCompleto());
	}

	@Override
	public List<Consulta> buscarFecha(LocalDateTime fecha) throws MediAppException {
		return repo.buscarFecha(fecha, fecha.plusDays(1));
	}

	@Override
	public List<ConsultaResumenDTO> listarResumen() throws MediAppException {
		List<ConsultaResumenDTO> consultas = new ArrayList<>();
		repo.listarResumen().forEach(x -> {
			ConsultaResumenDTO dto = new ConsultaResumenDTO();
			dto.setCantidad(Integer.parseInt(String.valueOf(x[0])));
			dto.setFecha(String.valueOf(x[1]));
			consultas.add(dto);
		});
		return consultas;
	}

	@Override
	public byte[] generarReporte() throws MediAppException {
		byte[] data = null;
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("txt_titulo", "Prueba de Titulo");

		try {
			File file = new ClassPathResource("/reports/consultas.jasper").getFile();
			JasperPrint print = JasperFillManager.fillReport(file.getPath(), parametros,
					new JRBeanCollectionDataSource(this.listarResumen()));

			data = JasperExportManager.exportReportToPdf(print);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}
}
