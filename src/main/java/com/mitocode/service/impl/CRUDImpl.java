package com.mitocode.service.impl;

import java.util.List;

import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.ICRUD;

public abstract class CRUDImpl<T, I> implements ICRUD<T, I> {

	protected abstract IGenericRepo<T, I> getRepo();

	@Override
	public T registrar(T t) {
		return getRepo().save(t);
	}

	@Override
	public List<T> listar() {
		return getRepo().findAll();
	}

	@Override
	public T listarPorId(I id) {
		return getRepo().findById(id).orElse(null);
	}

	@Override
	public T modificar(T t) {
		return getRepo().save(t);
	}

	@Override
	public void eliminar(I id) {
		getRepo().deleteById(id);
	}

}
