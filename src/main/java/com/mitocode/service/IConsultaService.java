package com.mitocode.service;

import java.time.LocalDateTime;
import java.util.List;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.exception.MediAppException;
import com.mitocode.model.Consulta;

public interface IConsultaService extends ICRUD<Consulta, Integer> {

	Consulta registrarConsultaTransaccional(ConsultaListaExamenDTO consulta) throws MediAppException;

	List<Consulta> buscar(FiltroConsultaDTO filtro) throws MediAppException;

	List<Consulta> buscarFecha(LocalDateTime fecha) throws MediAppException;

	List<ConsultaResumenDTO> listarResumen() throws MediAppException;

	byte[] generarReporte() throws MediAppException;

}
