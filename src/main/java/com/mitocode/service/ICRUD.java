package com.mitocode.service;

import java.util.List;

import com.mitocode.exception.MediAppException;

public interface ICRUD<T, I> {

	T registrar(T t); //throws MediAppException;

	List<T> listar() throws MediAppException;

	T listarPorId(I id) throws MediAppException;

	T modificar(T t) throws MediAppException;

	void eliminar(I id) throws MediAppException;
}
