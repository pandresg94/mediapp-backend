package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.MediAppException;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/signos")
public class SignosController {

	@Autowired
	ISignosService service;

	@GetMapping
	public ResponseEntity<List<Signos>> listar() throws MediAppException {
		List<Signos> result = service.listar();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Consulta paciente por Id con HATEOAS")
	@GetMapping("hateoas/{id}")
	public EntityModel<Signos> listarPorIdHateOas(@PathVariable Integer id) throws MediAppException {
		Signos obj = service.listarPorId(id);

		if (Objects.isNull(obj))
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);

		EntityModel<Signos> recurso = EntityModel.of(obj);
		WebMvcLinkBuilder link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).listarPorId(id));

		recurso.add(link.withRel("signo-recurso"));

		return recurso;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Signos> listarPorId(@PathVariable Integer id) throws MediAppException {
		Signos obj = service.listarPorId(id);

		if (Objects.isNull(obj))
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Signos> registrar(@Valid @RequestBody Signos obj) throws MediAppException {
		Signos objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@PostMapping("/registrarSigno")
	public ResponseEntity<Signos> registrarPaciente(@Valid @RequestBody Signos obj) throws MediAppException {
		Signos objSave = service.registrar(obj);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(objSave.getIdSigno()).toUri();

		return ResponseEntity.created(location).body(objSave);
	}

	@PutMapping
	public ResponseEntity<Signos> modificar(@RequestBody Signos obj) throws MediAppException {
		Signos objSave = service.modificar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@GetMapping("/pageable")
	public ResponseEntity<Page<Signos>> listarPageable(Pageable pageable) throws Exception {
		Page<Signos> signos = service.signosPaginado(pageable);
		return new ResponseEntity<Page<Signos>>(signos, HttpStatus.OK);
	}
}
