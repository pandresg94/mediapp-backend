package com.mitocode.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mitocode.dto.ConsultaListaExamenDTO;
import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.exception.MediAppException;
import com.mitocode.model.Archivo;
import com.mitocode.model.Consulta;
import com.mitocode.service.IArchivoService;
import com.mitocode.service.IConsultaService;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

	@Autowired
	private IConsultaService service;

	@Autowired
	private IArchivoService serviceArchivo;

	@GetMapping
	public ResponseEntity<List<Consulta>> listar() throws MediAppException {
		List<Consulta> result = service.listar();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Consulta> listarPorId(@PathVariable Integer id) throws MediAppException {
		Consulta obj = service.listarPorId(id);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Consulta> registrar(@RequestBody ConsultaListaExamenDTO obj) throws MediAppException {
		try {
			Consulta objSave = service.registrarConsultaTransaccional(obj);
			return new ResponseEntity<>(objSave, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@PutMapping
	public ResponseEntity<Consulta> modificar(@RequestBody Consulta obj) throws MediAppException {
		Consulta objSave = service.modificar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@GetMapping("/buscar")
	public ResponseEntity<List<Consulta>> buscarFecha(@RequestParam("fecha") String fecha) throws MediAppException {
		List<Consulta> result = new ArrayList<>();

		result = service.buscarFecha(LocalDateTime.parse(fecha));

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping("/buscar/otros")
	public ResponseEntity<List<Consulta>> buscarFecha(@RequestBody FiltroConsultaDTO dto) throws MediAppException {
		List<Consulta> result = new ArrayList<>();

		result = service.buscar(dto);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/listarResumen")
	public ResponseEntity<List<ConsultaResumenDTO>> listarResumen() throws MediAppException {
		List<ConsultaResumenDTO> result = new ArrayList<>();

		result = service.listarResumen();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping(value = "/generarReporte", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporte() throws MediAppException {
		byte[] result = null;

		result = service.generarReporte();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@PostMapping(value = "/guardarArchivo", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<Integer> guardarArchivo(@RequestParam("adjunto") MultipartFile file)
			throws MediAppException, IOException {
		Integer rpta = 0;

		Archivo ar = new Archivo();
		ar.setFiletype(file.getContentType());
		ar.setFilename(file.getOriginalFilename());
		ar.setValue(file.getBytes());

		rpta = serviceArchivo.guardar(ar);
		return new ResponseEntity<>(rpta, HttpStatus.OK);
	}

	@GetMapping(value = "/leerArchivo/{idArchivo}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> leerArchivo(@PathVariable("idArchivo") Integer idArchivo) throws IOException {

		byte[] arr = serviceArchivo.leerArchivo(idArchivo);

		return new ResponseEntity<byte[]>(arr, HttpStatus.OK);
	}
}
