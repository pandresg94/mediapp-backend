package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.MediAppException;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

	@Autowired
	private IPacienteService service;

	@GetMapping
	public ResponseEntity<List<Paciente>> listar() throws MediAppException {
		List<Paciente> result = service.listar();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@Operation(summary = "Consulta paciente por Id con HATEOAS")
	@GetMapping("hateoas/{id}")
	public EntityModel<Paciente> listarPorIdHateOas(@PathVariable Integer id) throws MediAppException {
		Paciente obj = service.listarPorId(id);

		if (Objects.isNull(obj))
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);

		EntityModel<Paciente> recurso = EntityModel.of(obj);
		WebMvcLinkBuilder link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).listarPorId(id));

		recurso.add(link.withRel("paciente-recurso"));

		return recurso;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Paciente> listarPorId(@PathVariable Integer id) throws MediAppException {
		Paciente obj = service.listarPorId(id);

		if (Objects.isNull(obj))
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Paciente> registrar(@Valid @RequestBody Paciente obj) throws MediAppException {
		Paciente objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@PostMapping("/registrarPaciente")
	public ResponseEntity<Paciente> registrarPaciente(@Valid @RequestBody Paciente obj) throws MediAppException {
		Paciente objSave = service.registrar(obj);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(objSave.getIdPaciente()).toUri();

		return ResponseEntity.created(location).body(objSave);
	}

	@PutMapping
	public ResponseEntity<Paciente> modificar(@RequestBody Paciente obj) throws MediAppException {
		Paciente objSave = service.modificar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@GetMapping("/pageable")
	public ResponseEntity<Page<Paciente>> listarPageable(Pageable pageable) throws Exception {
		Page<Paciente> pacientes = service.listarPageable(pageable);
		return new ResponseEntity<Page<Paciente>>(pacientes, HttpStatus.OK);
	}
}
