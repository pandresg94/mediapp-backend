package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.MediAppException;
import com.mitocode.model.Especialidad;
import com.mitocode.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

	@Autowired
	private IEspecialidadService service;

	@GetMapping
	public ResponseEntity<List<Especialidad>> listar() throws MediAppException {
		List<Especialidad> result = service.listar();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Especialidad> listarPorId(@PathVariable Integer id) throws MediAppException {
		Especialidad obj = service.listarPorId(id);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Especialidad> registrar(@RequestBody Especialidad obj) throws MediAppException {
		Especialidad objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Especialidad> modificar(@RequestBody Especialidad obj) throws MediAppException {
		Especialidad objSave = service.modificar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}
}
