package com.mitocode.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.MediAppException;
import com.mitocode.model.Examen;
import com.mitocode.service.IExamenService;

@RestController
@RequestMapping("/examenes")
public class ExamenController {

	@Autowired
	private IExamenService service;

	@GetMapping
	public ResponseEntity<List<Examen>> listar() throws MediAppException {
		List<Examen> result = service.listar();

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Examen> listarPorId(@PathVariable Integer id) throws MediAppException {
		Examen obj = service.listarPorId(id);
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Examen> registrar(@RequestBody Examen obj) throws MediAppException {
		Examen objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Examen> modificar(@RequestBody Examen obj) throws MediAppException {
		Examen objSave = service.modificar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}
}
